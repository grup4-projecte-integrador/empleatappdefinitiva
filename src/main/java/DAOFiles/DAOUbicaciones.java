/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import java.util.List;
import modelos.BatoilogicCliente;
import modelos.BatoilogicUbicacion;
import org.apache.xmlrpc.XmlRpcException;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class DAOUbicaciones {

    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private Session session;

    public DAOUbicaciones() throws XmlRpcException, MalformedURLException {

        session = NewHibernateUtil.getSessionFactory().openSession();
    }

    public BatoilogicUbicacion getClientes(int id) {

        List<BatoilogicUbicacion> l = session.createQuery("select a from BatoilogicUbicacion a where a.batoilogicRepartidor = " + id).list();

        if (l.size() < 1) {
            return null;
        }

        return l.get(l.size() - 1);

    }
}

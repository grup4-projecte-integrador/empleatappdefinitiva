/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import modelos.BatoilogicAlbaran;
import modelos.BatoilogicRuta;
import org.apache.xmlrpc.XmlRpcException;
import org.hibernate.Session;

public class DAOAlbaran {

    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private Session session;

    public DAOAlbaran() throws XmlRpcException, MalformedURLException {

        session = NewHibernateUtil.getSessionFactory().openSession();
    }

    public boolean insert(BatoilogicAlbaran t) throws Exception {
        session = NewHibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();

        if (session.isOpen()) {
            session.close();
        }
        return true;
    }

    public boolean insertRuta(BatoilogicRuta t) throws Exception {
        session = NewHibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();

        if (session.isOpen()) {
            session.close();
        }
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.ConexionBD;
import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import java.util.List;
import modelos.BatoilogicCamion;
import modelos.BatoilogicPedidoprov;
import modelos.BatoilogicProveedor;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.hibernate.Session;

public class DAOProveedores {

    private ConexionBD base = new ConexionBD();
    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private XmlRpcClient conexion;
    private Session session;

    public DAOProveedores() throws XmlRpcException, MalformedURLException {

        conexion = base.getInstance();
        session = NewHibernateUtil.getSessionFactory().openSession();
    }

    public List<BatoilogicProveedor> getProveedores(int id) {

        return session.createQuery("select a.batoilogicProveedor from BatoilogicPertenecer a where a.batoilogicProducto = " + id).list();

    }

    public Object getProveedor(String cif) {

        return session.createQuery("select a from BatoilogicProveedor a where a.cif = '" + cif + "'").list().get(0);

    }
    
    public boolean insertPedidoProv(Object t) throws Exception {
        BatoilogicPedidoprov artInsertar = (BatoilogicPedidoprov) t;
        session.beginTransaction();
        session.save(artInsertar);
        session.getTransaction().commit();
        return true;
    }

    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.ConexionBD;
import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import java.util.List;
import modelos.BatoilogicCamion;
import modelos.BatoilogicCliente;
import modelos.BatoilogicPedido;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class DAOClientes {

    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private Session session;

    public DAOClientes() throws XmlRpcException, MalformedURLException {

        session = NewHibernateUtil.getSessionFactory().openSession();
    }

    public List<BatoilogicCliente> getClientes() {

        return session.createQuery("select a from BatoilogicCliente a").list();

    }
    
    
}

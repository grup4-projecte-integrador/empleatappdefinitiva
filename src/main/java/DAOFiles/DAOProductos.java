/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.ConexionBD;
import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import java.util.List;
import modelos.BatoilogicLineaspedido;
import modelos.BatoilogicPedido;
import modelos.BatoilogicProducto;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.hibernate.Session;

public class DAOProductos {

    private ConexionBD base = new ConexionBD();
    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private XmlRpcClient conexion;
    private Session session;

    public DAOProductos() throws XmlRpcException, MalformedURLException {

        conexion = base.getInstance();
        session = NewHibernateUtil.getSessionFactory().openSession();

    }

    public List<BatoilogicProducto> getProductos() {

        return session.createQuery("select a from BatoilogicProducto a").list();
    }

    public List<BatoilogicProducto> getProductosStock0() {

        return session.createQuery("select a from BatoilogicProducto a where a.stock <= 0").list();
    }

    public List<BatoilogicLineaspedido> getLineasDependeProductos(int id) {

        return session.createQuery("select a from BatoilogicLineaspedido a where a.batoilogicProducto.id = " + id).list();
    }

    public boolean update(BatoilogicProducto t) throws Exception {
        BatoilogicProducto producto = (BatoilogicProducto) t;
        BatoilogicProducto productoAuxiliar = (BatoilogicProducto) findByPKProducto(producto.getId());
        session.getTransaction().begin();
        productoAuxiliar.setStock(producto.getStock());
        session.getTransaction().commit();
        return true;
    }

    public Object findByPKPedido(int id) throws Exception {
        return session.get(BatoilogicPedido.class, id);
    }

    public Object findByPKProducto(int id) throws Exception {
        return session.get(BatoilogicProducto.class, id);
    }

    public boolean update(BatoilogicPedido t) throws Exception {
        BatoilogicPedido pedido = (BatoilogicPedido) t;
        BatoilogicPedido pedidoActualizar = (BatoilogicPedido) findByPKPedido(pedido.getId());
        session.getTransaction().begin();
        pedidoActualizar.setEstado(pedido.getEstado());
        session.getTransaction().commit();
        return true;
    }

}

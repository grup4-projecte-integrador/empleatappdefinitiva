/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.ConexionBD;
import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import modelos.BatoilogicCamion;
import modelos.BatoilogicProducto;
import modelos.BatoilogicRepartidor;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.hibernate.Session;

/**
 *
 * @author batoi
 */
public class DAOCamiones {

    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private Session session;

    public DAOCamiones() throws XmlRpcException, MalformedURLException {

        session = NewHibernateUtil.getSessionFactory().openSession();
    }

    public int getRepartidorId(int camionId) {

        List<BatoilogicRepartidor> repartidores = session.createQuery("select a from BatoilogicRepartidor a where a.batoilogicCamion.id = " + camionId).list();

        return repartidores.get(0).getId();

    }

    public List<BatoilogicCamion> getCamiones() {

        return session.createQuery("select a from BatoilogicCamion a").list();

    }

    public List<BatoilogicRepartidor> getRepartidores() {

        return session.createQuery("select a from BatoilogicRepartidor a").list();

    }

    public Object getRepartidor(String dni) {

        return session.createQuery("select a from BatoilogicRepartidor a where a.dni = '" + dni + "'").list().get(0);
    }

    public Object findByPKRepartidor(int id) throws Exception {
        return session.get(BatoilogicRepartidor.class, id);
    }

    public Object findByPKCamion(int id) throws Exception {
        return session.get(BatoilogicCamion.class, id);
    }

    public boolean update(Object t) throws Exception {
        BatoilogicRepartidor repartidor = (BatoilogicRepartidor) t;
        BatoilogicRepartidor repartidorAuxiliar = (BatoilogicRepartidor) findByPKRepartidor(repartidor.getId());
        session.getTransaction().begin();
        repartidorAuxiliar.getBatoilogicCamion().setCapacidad(repartidor.getBatoilogicCamion().getCapacidad());
        session.getTransaction().commit();
        return true;
    }

    public boolean update(BatoilogicCamion t) throws Exception {
        BatoilogicCamion camion = (BatoilogicCamion) t;
        BatoilogicCamion camionAuxiliar = (BatoilogicCamion) findByPKCamion(camion.getId());
        session.getTransaction().begin();
        camionAuxiliar.setCapacidad(camion.getCapacidad());
        session.getTransaction().commit();
        return true;
    }

}

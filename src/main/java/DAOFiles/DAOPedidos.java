/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFiles;

import conexion.ConexionBD;
import conexion.NewHibernateUtil;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;
import modelos.BatoilogicLineaspedido;
import modelos.BatoilogicPedido;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.hibernate.Session;

public class DAOPedidos {

    private ConexionBD base = new ConexionBD();
    private NewHibernateUtil hibernate = new NewHibernateUtil();
    private XmlRpcClient conexion;
    private Session session;

    public DAOPedidos() throws XmlRpcException, MalformedURLException {

        conexion = base.getInstance();
        session = NewHibernateUtil.getSessionFactory().openSession();

    }

    public List<BatoilogicPedido> getPedidos() {

        return session.createQuery("select a from BatoilogicPedido a").list();

    }

    public List<BatoilogicPedido> getClienteFiltro(String user, String minima, String maxima) {

        return session.createQuery("select a from BatoilogicPedido a where a.batoilogicCliente.user = '" + user + "' and a.fechapedido between '" + minima + "' and '" + maxima + "'").list();

    }

    public List<BatoilogicPedido> getClienteFiltroSinUsuario(String minima, String maxima) {

        return session.createQuery("select a from BatoilogicPedido a where a.fechapedido between '" + minima + "' and '" + maxima + "'").list();

    }
    
    public List<BatoilogicLineaspedido> getLineasPedido(int id) {

        return session.createQuery("select a from BatoilogicLineaspedido a where a.batoilogicPedido = " + id).list();

    }
    
    public Object findByPK(int id) throws Exception {
        return session.get(BatoilogicLineaspedido.class, id);
    }

    public boolean update(BatoilogicLineaspedido t) throws Exception {
        BatoilogicLineaspedido pedido = (BatoilogicLineaspedido) t;
        BatoilogicLineaspedido pedidoActualizar = (BatoilogicLineaspedido) findByPK(pedido.getId());
        session.getTransaction().begin();
        pedidoActualizar.setBatoilogicProducto(pedido.getBatoilogicProducto());
        session.getTransaction().commit();
        return true;
    }

    

}

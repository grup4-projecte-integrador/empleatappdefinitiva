package controladores;

import DAOFiles.DAOAlbaran;
import DAOFiles.DAOCamiones;
import DAOFiles.DAOClientes;
import DAOFiles.DAOPedidos;
import DAOFiles.DAOProductos;
import DAOFiles.DAOProveedores;
import DAOFiles.DAOUbicaciones;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import modelos.BatoilogicAlbaran;
import modelos.BatoilogicCamion;
import modelos.BatoilogicCliente;
import modelos.BatoilogicLineaspedido;
import modelos.BatoilogicPedido;
import modelos.BatoilogicPedidoprov;
import modelos.BatoilogicProducto;
import modelos.BatoilogicProveedor;
import modelos.BatoilogicRepartidor;
import modelos.BatoilogicRuta;
import modelos.BatoilogicUbicacion;
import org.apache.xmlrpc.XmlRpcException;

public class MenuController implements Initializable {

    private DAOCamiones cdao;
    private DAOPedidos pdao;
    private DAOProductos prodao;
    private DAOClientes clidao;
    private DAOProveedores provdao;
    private DAOAlbaran adao;

    private ObservableList<BatoilogicLineaspedido> lineas;

    @FXML
    public AnchorPane menuCamiones;
    @FXML
    public AnchorPane menuPedidos;
    @FXML
    public AnchorPane menuStock;
    @FXML
    public AnchorPane menuInicio;

    @FXML
    private Text tvHora;
    @FXML
    private TableView<BatoilogicCamion> tableCamiones;
    @FXML
    private TableColumn<BatoilogicCamion, String> columnMatricula;
    @FXML
    private TableColumn<BatoilogicCamion, String> columnMarca;
    @FXML
    private TableColumn<BatoilogicCamion, String> columnModelo;
    @FXML
    private TableColumn<BatoilogicCamion, Integer> columnCapacidad;

    @FXML
    private TableView<BatoilogicProducto> tableProductos;
    @FXML
    private TableColumn<BatoilogicProducto, String> columnNombre;
    @FXML
    private TableColumn<BatoilogicProducto, String> columnDescripcionProducto;
    @FXML
    private TableColumn<BatoilogicProducto, String> columnPrecio;
    @FXML
    private TableColumn<BatoilogicProducto, String> columnStock;

    @FXML
    private TableView<BatoilogicPedido> tablePedidos;
    @FXML
    private TableColumn<BatoilogicPedido, String> columnCliente;
    @FXML
    private TableColumn<BatoilogicPedido, String> columnObservaciones;
    @FXML
    private TableColumn<BatoilogicPedido, String> columnFechaEstimada;
    @FXML
    private TableColumn<BatoilogicPedido, String> columnFechaPedido;
    @FXML
    private TableColumn<BatoilogicPedido, String> columnEstado;
    @FXML
    private TableColumn<BatoilogicPedido, String> columnPrecioTotal;

    @FXML
    private TableView<BatoilogicLineaspedido> tableLineas;
    @FXML
    private TableColumn<BatoilogicLineaspedido, String> columnNombreProducto;
    @FXML
    private TableColumn<BatoilogicLineaspedido, Integer> columnCantidad;
    @FXML
    private TableColumn<BatoilogicLineaspedido, String> columnStockDisponible;

    @FXML
    private DatePicker dpDesde;
    @FXML
    private DatePicker dpHasta;
    @FXML
    private ComboBox<String> cbClientePedidos;

    @FXML
    private WebView mapView;
    private WebEngine webEngine;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            webEngine = mapView.getEngine();
            cdao = new DAOCamiones();
            pdao = new DAOPedidos();
            prodao = new DAOProductos();
            clidao = new DAOClientes();
            provdao = new DAOProveedores();
            adao = new DAOAlbaran();
            menuInicio.toFront();

        } catch (XmlRpcException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void abrirMenuCamiones() {
        menuCamiones.toFront();
        ObservableList<BatoilogicCamion> camiones = FXCollections.observableArrayList(cdao.getCamiones());

        tableCamiones.setItems(camiones);

        columnMatricula.setCellValueFactory(cellData -> cellData.getValue().getMatriculaProperty());
        columnMarca.setCellValueFactory(cellData -> cellData.getValue().getMarcaProperty());
        columnModelo.setCellValueFactory(cellData -> cellData.getValue().getModeloProperty());
        columnCapacidad.setCellValueFactory(new PropertyValueFactory<>("capacidad"));

    }

    public void abrirMenuPedidos() {
        menuPedidos.toFront();

        ObservableList<BatoilogicPedido> pedidos = FXCollections.observableArrayList(pdao.getPedidos());
        ObservableList<BatoilogicCliente> clientes = FXCollections.observableArrayList(clidao.getClientes());

        if (pedidos.size() > 0) {
            tablePedidos.setItems(pedidos);
            columnCliente.setCellValueFactory(cellData -> cellData.getValue().getDescripcionProperty());
            columnObservaciones.setCellValueFactory(cellData -> cellData.getValue().getObservacionProperty());
            columnFechaEstimada.setCellValueFactory(cellData -> cellData.getValue().getFechaestimadaProperty());
            columnFechaPedido.setCellValueFactory(cellData -> cellData.getValue().getFechapedidoProperty());
            columnEstado.setCellValueFactory(cellData -> cellData.getValue().getEstadoProperty());
            columnPrecioTotal.setCellValueFactory(cellData -> cellData.getValue().getPrecioProperty());

            tablePedidos.refresh();
            tableLineas.refresh();
            ObservableList<String> clientesRegistrados = FXCollections.observableArrayList();

            Date fechaMinima = new Date();
            Date fechaMaxima = new Date();

            fechaMaxima = pedidos.get(0).getFechapedido();

            for (int i = 0; i < pedidos.size(); i++) {

                if (pedidos.get(i).getFechapedido().before(fechaMinima)) {

                    fechaMinima = pedidos.get(i).getFechapedido();
                }

            }

            for (int i = 1; i < pedidos.size(); i++) {

                if (pedidos.get(i).getFechapedido().after(fechaMinima)) {

                    fechaMaxima = pedidos.get(i).getFechapedido();
                }

            }

            for (int i = 0; i < clientes.size(); i++) {
                clientesRegistrados.add(clientes.get(i).getNombre() + " - " + clientes.get(i).getUser());

            }

            LocalDate minima = Instant.ofEpochMilli(fechaMinima.getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

            LocalDate maxima = Instant.ofEpochMilli(fechaMaxima.getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

            dpDesde.setValue(minima);
            dpHasta.setValue(maxima);
            cbClientePedidos.setItems(clientesRegistrados);

        }

    }

    public void abrirMenuStock() {
        menuStock.toFront();

        ObservableList<BatoilogicProducto> productos = FXCollections.observableArrayList(prodao.getProductos());

        tableProductos.setItems(productos);

        columnNombre.setCellValueFactory(cellData -> cellData.getValue().getNombreProperty());
        columnDescripcionProducto.setCellValueFactory(cellData -> cellData.getValue().getDescripcionProperty());
        columnPrecio.setCellValueFactory(cellData -> cellData.getValue().getPrecioProperty());
        columnStock.setCellValueFactory(cellData -> cellData.getValue().getStockProperty());
    }

    public void abrirMenuInicio() {
        menuInicio.toFront();

    }

    public void cambiarRepartidor(MouseEvent e) {

        BatoilogicCamion camionSeleccionado = tableCamiones.getSelectionModel().getSelectedItem();

        if (e.getClickCount() == 1) {
            try {
                int repartidorId = new DAOCamiones().getRepartidorId(camionSeleccionado.getId());

                BatoilogicUbicacion ubicacion = new DAOUbicaciones().getClientes(repartidorId);
                if (ubicacion != null) {
                    double lat = ubicacion.getLatitud();
                    double lon = ubicacion.getLongitud();

                    String url = "https://www.google.es/maps/place/" + lat + "," + lon;

                    webEngine.load(url);
                    tvHora.setText("Ultima hora: " + ubicacion.getCreateDate());

                } else {

                    crearAlertaError("Este camion no tiene una ubicacion disponible");
                }
            } catch (XmlRpcException ex) {
                Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    public void pedirStock(MouseEvent e) {

        BatoilogicProducto producto = tableProductos.getSelectionModel().getSelectedItem();

        if (e.getClickCount() == 2 && producto != null) {

            List<BatoilogicProveedor> repartidores = provdao.getProveedores(producto.getId());

            List<String> proveedoresName = new ArrayList<>();

            for (int i = 0; i < repartidores.size(); i++) {

                proveedoresName.add(repartidores.get(i).getCif() + " - " + repartidores.get(i).getTelefono());
            }

            try {
                TextInputDialog tid = new TextInputDialog();
                tid.setTitle("Introduce la cantidad de stock que quieres pedir: ");
                Optional<String> result = tid.showAndWait();

                if (!result.isPresent() || tid.getEditor().getText().trim().length() == 0) {

                    throw new Exception("Debes introducir una cantidad");
                }
                int cantidad = Integer.parseInt(tid.getEditor().getText());

                ChoiceDialog<String> dialog = new ChoiceDialog<>(proveedoresName.get(0), proveedoresName);
                dialog.setTitle("Elige su proveedor");
                dialog.setHeaderText("Se pedira el stock al proveedor que elijas ");
                dialog.setContentText("Elige al proveedor");
                Optional<String> salida = dialog.showAndWait();

                if (!result.isPresent() || dialog.getSelectedItem() == null) {

                    throw new Exception("Debes elegir un proveedor");
                }

                TextInputDialog desc = new TextInputDialog();
                desc.setTitle("Introduce una descripcion ");
                Optional<String> resultDesc = desc.showAndWait();

                if (!result.isPresent() || desc.getEditor().getText().trim().length() == 0) {

                    throw new Exception("Debes introducir una descripcion");
                }

                if (result.isPresent() || !salida.isEmpty() || resultDesc.isPresent()) {

                    if (cantidad > 0) {
                        String proveedorSeleccionado = dialog.getSelectedItem();
                        String[] seleccion = proveedorSeleccionado.split(" - ");

                        BatoilogicProveedor proveedor = (BatoilogicProveedor) provdao.getProveedor(seleccion[0]);
                        BatoilogicPedidoprov prov = new BatoilogicPedidoprov();

                        prov.setBatoilogicProducto(producto);
                        prov.setBatoilogicProveedor(proveedor);
                        prov.setDescripcion(desc.getEditor().getText());

                        producto.setStock(producto.getStock() + cantidad);
                        List<BatoilogicLineaspedido> lineasPedido = prodao.getLineasDependeProductos(producto.getId());

                        for (int i = 0; i < lineasPedido.size(); i++) {
                            BatoilogicLineaspedido linea = lineasPedido.get(i);

                            linea.setBatoilogicProducto(producto);
                            pdao.update(linea);

                        }

                        provdao.insertPedidoProv(prov);
                        prodao.update(producto);
                        tableProductos.refresh();

                    } else {

                        crearAlertaError("Debes introducir una cantidad mayor a 0");

                    }
                }

            } catch (Exception ex) {

                crearAlertaError(ex.getMessage());
                ex.printStackTrace();
            }
        }

    }

    public void crearAlertaError(String mensaje) {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Comprueba lo que has introducido");
        alert.setContentText(mensaje);

        alert.showAndWait();

    }

    public void crearAlertaError(ArrayList<String> mensaje) {

        String mensajeFinal = "";

        for (int i = 0; i < mensaje.size(); i++) {

            mensajeFinal += mensaje.get(i) + System.lineSeparator();
        }

        mensajeFinal += "se debe pedir stock de estos productos";
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Comprueba lo que has introducido");
        alert.setContentText(mensajeFinal);

        alert.showAndWait();

    }

    public void filtrarItems() {

        ObservableList<BatoilogicProducto> productos = FXCollections.observableArrayList(prodao.getProductosStock0());

        if (!productos.isEmpty()) {
            tableProductos.setItems(productos);

            columnNombre.setCellValueFactory(cellData -> cellData.getValue().getNombreProperty());
            columnDescripcionProducto.setCellValueFactory(cellData -> cellData.getValue().getDescripcionProperty());
            columnPrecio.setCellValueFactory(cellData -> cellData.getValue().getPrecioProperty());
            columnStock.setCellValueFactory(cellData -> cellData.getValue().getStockProperty());

        } else {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("No hay productos sin stock");
            alert.setHeaderText("Todas los productos tienen stock suficiente");
            alert.setContentText("Recuerda volver hacer esta consulta para comprobar el stock");

            alert.showAndWait();

        }
    }

    public void filtrarPedidos() {

        LocalDate minima = dpDesde.getValue();
        LocalDate maxima = dpHasta.getValue();

        Date minDate = Date.from(minima.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Date maxDate = Date.from(maxima.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        if (cbClientePedidos.getValue() != null) {
            String[] clienteSeleccionado = cbClientePedidos.getValue().toString().split(" - ");

            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
            String dateMinima = form.format(minDate);
            String dateMaxima = form.format(maxDate);

            ObservableList<BatoilogicPedido> pedidos = FXCollections.observableArrayList(pdao.getClienteFiltro(clienteSeleccionado[1], dateMinima, dateMaxima));

            tablePedidos.setItems(pedidos);

            columnCliente.setCellValueFactory(cellData -> cellData.getValue().getDescripcionProperty());
            columnObservaciones.setCellValueFactory(cellData -> cellData.getValue().getObservacionProperty());
            columnFechaEstimada.setCellValueFactory(cellData -> cellData.getValue().getFechaestimadaProperty());
            columnFechaPedido.setCellValueFactory(cellData -> cellData.getValue().getFechapedidoProperty());
            columnEstado.setCellValueFactory(cellData -> cellData.getValue().getEstadoProperty());
            columnPrecioTotal.setCellValueFactory(cellData -> cellData.getValue().getPrecioProperty());

        } else {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("No has seleccionado usuario");
            alert.setHeaderText("Solo se van a filtrar por fecha");
            alert.setContentText("Si deseas filtrar por cliente seleccionalo");

            alert.showAndWait();

            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
            String dateMinima = form.format(minDate);
            String dateMaxima = form.format(maxDate);

            ObservableList<BatoilogicPedido> pedidos = FXCollections.observableArrayList(pdao.getClienteFiltroSinUsuario(dateMinima, dateMaxima));

            tablePedidos.setItems(pedidos);

            columnCliente.setCellValueFactory(cellData -> cellData.getValue().getDescripcionProperty());
            columnObservaciones.setCellValueFactory(cellData -> cellData.getValue().getObservacionProperty());
            columnFechaEstimada.setCellValueFactory(cellData -> cellData.getValue().getFechaestimadaProperty());
            columnFechaPedido.setCellValueFactory(cellData -> cellData.getValue().getFechapedidoProperty());
            columnEstado.setCellValueFactory(cellData -> cellData.getValue().getEstadoProperty());
            columnPrecioTotal.setCellValueFactory(cellData -> cellData.getValue().getPrecioProperty());

        }
    }

    public void compararStock() {

        BatoilogicPedido pedido = tablePedidos.getSelectionModel().getSelectedItem();
        BatoilogicRepartidor repartidor = new BatoilogicRepartidor();
        ArrayList<String> productosSinStock = new ArrayList<>();

        if (pedido != null && lineas.size() > 0 && pedido.getEstado().equalsIgnoreCase("EP")) {

            boolean semaforo = true;
            for (int i = 0; i < lineas.size(); i++) {

                if (lineas.get(i).getStockProducto() < lineas.get(i).getCantidad()) {

                    semaforo = false;
                    productosSinStock.add(lineas.get(i).getNombreProducto());

                }

            }

            if (semaforo) {

                try {
                    List<BatoilogicRepartidor> repartidores = cdao.getRepartidores();

                    List<String> repartidoresName = new ArrayList<>();

                    for (int i = 0; i < repartidores.size(); i++) {

                        repartidoresName.add(repartidores.get(i).getNombre() + " - " + repartidores.get(i).getDni());
                    }

                    ChoiceDialog<String> dialog = new ChoiceDialog<>(repartidoresName.get(0), repartidoresName);
                    dialog.setTitle("Elige repartidor");
                    dialog.setHeaderText("El repartidor que eligas se le asignara el albaran que se va a crear");
                    dialog.setContentText("Elige al repartidor");
                    Optional<String> salida = dialog.showAndWait();

                    if (!dialog.getSelectedItem().isEmpty() && salida.isPresent()) {

                        String posicion = dialog.getSelectedItem();
                        String[] seleccion = posicion.split(" - ");
                        repartidor = (BatoilogicRepartidor) cdao.getRepartidor(seleccion[1]);

                        if (repartidor.getBatoilogicCamion().getCapacidad() < 30) {

                            BatoilogicAlbaran batoilogicAlbaran = new BatoilogicAlbaran();
                            BatoilogicRuta ruta = new BatoilogicRuta();

                            ruta.setBatoilogicRepartidor(repartidor);

                            batoilogicAlbaran.setBatoilogicPedido(pedido);
                            batoilogicAlbaran.setBatoilogicRuta(ruta);
                            BatoilogicCamion camion = (BatoilogicCamion) cdao.findByPKCamion(repartidor.getBatoilogicCamion().getId());
                            int nuevaCapacidad = repartidor.getBatoilogicCamion().getCapacidad() + 1;
                            camion.setCapacidad(nuevaCapacidad);
                            repartidor.getBatoilogicCamion().setCapacidad(nuevaCapacidad);

                            cdao.update(repartidor);
                            cdao.update(camion);
                            adao.insertRuta(ruta);
                            adao.insert(batoilogicAlbaran);

                            pedido.setEstado("Preparado");
                            prodao.update(pedido);
                            tablePedidos.refresh();

                            for (int i = 0; i < lineas.size(); i++) {

                                BatoilogicLineaspedido linea = lineas.get(i);
                                int stock = linea.getBatoilogicProducto().getStock();
                                stock = linea.getBatoilogicProducto().getStock() - linea.getCantidad();
                                BatoilogicProducto producto = linea.getBatoilogicProducto();

                                linea.getBatoilogicProducto().setStock(stock);
                                producto.setStock(stock);

                                prodao.update(producto);
                                pdao.update(linea);

                            }

                            tableLineas.refresh();
                            tableCamiones.refresh();

                        } else {

                            crearAlertaError("Este repartidor ya tiene el camion lleno");

                        }
                    }
                } catch (Exception ex) {

                    ex.printStackTrace();
                }

            } else {

                crearAlertaError(productosSinStock);
            }
        }

    }

    public void mostrarLineas(MouseEvent e) {

        BatoilogicPedido pedido = tablePedidos.getSelectionModel().getSelectedItem();

        if (e.getClickCount() == 1 && pedido != null) {

            lineas = FXCollections.observableArrayList(pdao.getLineasPedido(pedido.getId()));
            tableLineas.setItems(lineas);
            columnNombreProducto.setCellValueFactory(cellData -> cellData.getValue().getPropertyNombreProducto());
            columnCantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
            columnStockDisponible.setCellValueFactory(cellData -> cellData.getValue().getPropertyStockProducto());
            tableLineas.refresh();

        }

    }

    public void insertarCSV() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File file = fileChooser.showOpenDialog((Stage) tableProductos.getScene().getWindow());

        if (file != null) {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                String aux = "";

                while ((aux = br.readLine()) != null) {
                    String datos[] = aux.split(",");

                    try {

                        BatoilogicProducto producto = (BatoilogicProducto) prodao.findByPKProducto(Integer.parseInt(datos[0]));
                        producto.setPrecio(BigDecimal.valueOf(Double.parseDouble(datos[1])));
                        prodao.update(producto);

                    } catch (Exception e) {
                        System.err.println(e);
                    }
                }

                tableProductos.refresh();
                Alert d = new Alert(Alert.AlertType.INFORMATION, "Los articulos se han insertado bien");
                d.setTitle("Inserccion");
                d.showAndWait();

            } catch (FileNotFoundException ex) {
                System.err.println(ex);
                Alert d = new Alert(Alert.AlertType.ERROR, "Error al insertar los Articulos");
                d.setTitle("Error");
                d.showAndWait();
            } catch (IOException ex) {
                System.err.println(ex);
                Alert d = new Alert(Alert.AlertType.ERROR, "Error al insertar los Articulos");
                d.setTitle("Error");
                d.showAndWait();
            } catch (Exception e) {
                System.err.println(e);
                Alert d = new Alert(Alert.AlertType.ERROR, "Error al insertar los Articulos");
                d.setTitle("Error");
                d.showAndWait();
            }
        } else {
            Alert d = new Alert(Alert.AlertType.ERROR, "Error al insertar los Articulos");
            d.setTitle("Error");
            d.showAndWait();
        }
    }

    public void salirApp() {

        ((Stage) menuStock.getScene().getWindow()).close();
    }

}

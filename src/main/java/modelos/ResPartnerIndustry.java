package modelos;
// Generated 14 feb. 2020 22:01:51 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * ResPartnerIndustry generated by hbm2java
 */
public class ResPartnerIndustry  implements java.io.Serializable {


     private int id;
     private ResUsers resUsersByWriteUid;
     private ResUsers resUsersByCreateUid;
     private String name;
     private String fullName;
     private Boolean active;
     private Date createDate;
     private Date writeDate;
     private Set resPartners = new HashSet(0);

    public ResPartnerIndustry() {
    }

	
    public ResPartnerIndustry(int id) {
        this.id = id;
    }
    public ResPartnerIndustry(int id, ResUsers resUsersByWriteUid, ResUsers resUsersByCreateUid, String name, String fullName, Boolean active, Date createDate, Date writeDate, Set resPartners) {
       this.id = id;
       this.resUsersByWriteUid = resUsersByWriteUid;
       this.resUsersByCreateUid = resUsersByCreateUid;
       this.name = name;
       this.fullName = fullName;
       this.active = active;
       this.createDate = createDate;
       this.writeDate = writeDate;
       this.resPartners = resPartners;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public ResUsers getResUsersByWriteUid() {
        return this.resUsersByWriteUid;
    }
    
    public void setResUsersByWriteUid(ResUsers resUsersByWriteUid) {
        this.resUsersByWriteUid = resUsersByWriteUid;
    }
    public ResUsers getResUsersByCreateUid() {
        return this.resUsersByCreateUid;
    }
    
    public void setResUsersByCreateUid(ResUsers resUsersByCreateUid) {
        this.resUsersByCreateUid = resUsersByCreateUid;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getFullName() {
        return this.fullName;
    }
    
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    public Boolean getActive() {
        return this.active;
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }
    public Set getResPartners() {
        return this.resPartners;
    }
    
    public void setResPartners(Set resPartners) {
        this.resPartners = resPartners;
    }




}



package modelos;
// Generated 14 feb. 2020 22:01:51 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * CrmTeam generated by hbm2java
 */
public class CrmTeam  implements java.io.Serializable {


     private int id;
     private IrAttachment irAttachment;
     private MailAlias mailAlias;
     private ResCompany resCompany;
     private ResUsers resUsersByWriteUid;
     private ResUsers resUsersByCreateUid;
     private ResUsers resUsersByUserId;
     private String name;
     private Integer sequence;
     private Boolean active;
     private Integer color;
     private Date createDate;
     private Date writeDate;
     private Boolean useLeads;
     private Boolean useOpportunities;
     private Set resPartners = new HashSet(0);
     private Set resUserses = new HashSet(0);

    public CrmTeam() {
    }

	
    public CrmTeam(int id, MailAlias mailAlias, String name) {
        this.id = id;
        this.mailAlias = mailAlias;
        this.name = name;
    }
    public CrmTeam(int id, IrAttachment irAttachment, MailAlias mailAlias, ResCompany resCompany, ResUsers resUsersByWriteUid, ResUsers resUsersByCreateUid, ResUsers resUsersByUserId, String name, Integer sequence, Boolean active, Integer color, Date createDate, Date writeDate, Boolean useLeads, Boolean useOpportunities, Set resPartners, Set resUserses) {
       this.id = id;
       this.irAttachment = irAttachment;
       this.mailAlias = mailAlias;
       this.resCompany = resCompany;
       this.resUsersByWriteUid = resUsersByWriteUid;
       this.resUsersByCreateUid = resUsersByCreateUid;
       this.resUsersByUserId = resUsersByUserId;
       this.name = name;
       this.sequence = sequence;
       this.active = active;
       this.color = color;
       this.createDate = createDate;
       this.writeDate = writeDate;
       this.useLeads = useLeads;
       this.useOpportunities = useOpportunities;
       this.resPartners = resPartners;
       this.resUserses = resUserses;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public IrAttachment getIrAttachment() {
        return this.irAttachment;
    }
    
    public void setIrAttachment(IrAttachment irAttachment) {
        this.irAttachment = irAttachment;
    }
    public MailAlias getMailAlias() {
        return this.mailAlias;
    }
    
    public void setMailAlias(MailAlias mailAlias) {
        this.mailAlias = mailAlias;
    }
    public ResCompany getResCompany() {
        return this.resCompany;
    }
    
    public void setResCompany(ResCompany resCompany) {
        this.resCompany = resCompany;
    }
    public ResUsers getResUsersByWriteUid() {
        return this.resUsersByWriteUid;
    }
    
    public void setResUsersByWriteUid(ResUsers resUsersByWriteUid) {
        this.resUsersByWriteUid = resUsersByWriteUid;
    }
    public ResUsers getResUsersByCreateUid() {
        return this.resUsersByCreateUid;
    }
    
    public void setResUsersByCreateUid(ResUsers resUsersByCreateUid) {
        this.resUsersByCreateUid = resUsersByCreateUid;
    }
    public ResUsers getResUsersByUserId() {
        return this.resUsersByUserId;
    }
    
    public void setResUsersByUserId(ResUsers resUsersByUserId) {
        this.resUsersByUserId = resUsersByUserId;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Integer getSequence() {
        return this.sequence;
    }
    
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
    public Boolean getActive() {
        return this.active;
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }
    public Integer getColor() {
        return this.color;
    }
    
    public void setColor(Integer color) {
        this.color = color;
    }
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }
    public Boolean getUseLeads() {
        return this.useLeads;
    }
    
    public void setUseLeads(Boolean useLeads) {
        this.useLeads = useLeads;
    }
    public Boolean getUseOpportunities() {
        return this.useOpportunities;
    }
    
    public void setUseOpportunities(Boolean useOpportunities) {
        this.useOpportunities = useOpportunities;
    }
    public Set getResPartners() {
        return this.resPartners;
    }
    
    public void setResPartners(Set resPartners) {
        this.resPartners = resPartners;
    }
    public Set getResUserses() {
        return this.resUserses;
    }
    
    public void setResUserses(Set resUserses) {
        this.resUserses = resUserses;
    }




}


